# TestMongo       
          
TestNG integration with MongoDB. Emulates TestNG DataProvider.    
      
### What is this repository for?      
    
* Execute TestNG tests with MongoDB backing up with data.
* A single TestNG testcase executed against [a as large as it can be] dataset.   
         
### How do I get started?       
        
* Run General/Input.java followed by Security/Page1/Input.Java in each test.* package to insert sample data         
* Run com.immanuelnoel.testmongo.core.TestMongo to run tests against the inserted sample data.            
         
### How do I use it to my benefit?       
         
* Start with adding tests. Testcase location can be defined in config.properties. The package / class name specified right after the package defined in config.properties will be treated as the MongoDB key.           
* Adding data for use in tests: Create a datastructure, and call "com.immanuelnoel.testmongo.core.dataHandler.Data.inputData" with an object of your datastructure as a parameter. See "com.immanuelnoel.testmongo.tests.General.Input.java" for an example.     
                
### Configuration:       
         
* Configurations are specified in config.properties    
	- Test base packages - Specify the base package under which test cases reside        
	- Tests to execute - Default '*'. Can be edited to include sub packages. Must end with * to execute sub directories          
	- Database connection details - MongoDB host and port to be specified here          
              
              