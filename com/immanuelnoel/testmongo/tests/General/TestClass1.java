package com.immanuelnoel.testmongo.tests.General;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.immanuelnoel.testmongo.core.TestBase;
import com.mongodb.BasicDBObject;

public class TestClass1 extends TestBase{
	
	@BeforeTest
	public void setTheContext() {
		super.setup();
	}
	
	@Test
	public void test1() {
		
		// Get data
		BasicDBObject currentData = getDataObject();
		
		// Sample test - verify username and password are equal
		String username = currentData.getString("username");
		String password = currentData.getString("password");
		Assert.assertTrue(username.compareTo(password) == 0, "Validated");
		
		System.out.println("Test1 Pass for UserName: " + username);
	}
	
	@Test
	public void test2() {
		
		// Get data
		BasicDBObject currentData = getDataObject();
		
		// Sample test - pass if username is admin0
		String username = currentData.getString("username");
		String password = currentData.getString("password");
		Assert.assertTrue(username.compareTo("admin0") == 0, "Validated");
	
		System.out.println("Test2 Pass for UserName: " + username);
	}

	
}
