package com.immanuelnoel.testmongo.tests.General;

import com.immanuelnoel.testmongo.core.dataHandler.Data;

public class Input {

	public static void main(String args[]) {
		
		// To simplify test
		// Delete all existing data in the collection
		Data.getInstance().deleteAllData();
		
		// Insert sample data
		// Adds data into a document with the name equal to the first level test package name
		for(int i = 0;  i < 3; i++) {
			
			DS dataItem = new DS();
			dataItem.setUserName("admin" + i);
			dataItem.setPassword("admin" + i);
			
			boolean result = Data.getInstance().inputData(dataItem);
			System.out.println("DBUpdate: " + result);
		}
	}
	
}
