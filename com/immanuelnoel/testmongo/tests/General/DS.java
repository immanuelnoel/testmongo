package com.immanuelnoel.testmongo.tests.General;

public class DS {
	public String username = "";
	public String password = "";
	
	public String getUserName() {
		return this.username;
	}
	public void setUserName(String _username) {
		this.username = _username;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String _password) {
		this.password = _password;
	}
}
