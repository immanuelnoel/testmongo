package com.immanuelnoel.testmongo.tests.Security.Page1;

import com.immanuelnoel.testmongo.core.dataHandler.Data;

public class Input {

	public static void main(String args[]) {
		
		// To simplify test
		// Delete all existing data in the collection
		Data.getInstance().deleteAllData();
		
		// Insert sample data
		// Adds data into a document with the name equal to the first level test package name
		for(int i = 0;  i < 3; i++) {
			
			DS dataItem = new DS();
			dataItem.setEmail("eee@ads.co" + i);
			dataItem.setPincode(5600 + i);
			
			boolean result = Data.getInstance().inputData(dataItem);
			System.out.println("DBUpdate: " + result);
		}
	}
	
}
