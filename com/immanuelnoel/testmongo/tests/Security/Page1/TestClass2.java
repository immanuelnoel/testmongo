package com.immanuelnoel.testmongo.tests.Security.Page1;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.immanuelnoel.testmongo.core.TestBase;
import com.mongodb.BasicDBObject;

public class TestClass2 extends TestBase{
	
	@BeforeTest
	public void setTheContext() {
		super.setup();
	}
	
	@Test
	public void test1() {
		
		// Get data
		BasicDBObject currentData = getDataObject();
		
		// Sample test - verify pincode is greater than 5600
		String email = currentData.getString("email");
		int pincode = currentData.getInt("pincode");
		Assert.assertTrue(pincode > 5600, "Validated");
		
		System.out.println("Test1 pass for email: " + email);
	}
	
	@AfterTest
	public void record(){
		
	}
	
	@Test
	public void test2() {
		
		// Get data
		BasicDBObject currentData = getDataObject();
		
		// Sample test - Test for @ in email
		String email = currentData.getString("email");
		int pincode = currentData.getInt("pincode");
		Assert.assertTrue(email.contains("@"), "Validated");
	
		System.out.println("Test2 pass for email: " + email);
	}

	
}
