package com.immanuelnoel.testmongo.core.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class InvocationListener implements IInvokedMethodListener {

	@Override
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {
		// TODO Auto-generated method stub
		
		if(arg0.isTestMethod()){
			arg1.setAttribute("currentInvocationCount", arg0.getTestMethod().getCurrentInvocationCount());
		}
	}
	
	@Override
	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {
		
	}
}
