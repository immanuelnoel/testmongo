package com.immanuelnoel.testmongo.core.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;
import com.immanuelnoel.testmongo.core.dataHandler.Data;

public class Invocator implements IAnnotationTransformer {
	
	// Called before every loop of function call
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod)
	{	
		
		// Get Data Object Counts
		Data dataObj = Data.getInstance(); 
		int invocationCount = dataObj.getDataCount(testMethod.getDeclaringClass().getName().toString());
		
		annotation.setInvocationCount(invocationCount); 
	}
}


