package com.immanuelnoel.testmongo.core.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * TestMongo
 * @author immanuelnoel
 *
 */
public class PropertiesReader 
{
	private Properties applicationProps = new Properties();

    public PropertiesReader(String filePath)
    {
        try 
        {
            InputStream is = new FileInputStream(filePath);
            applicationProps.load(is);
            is.close();
        }
        catch(FileNotFoundException fnfe)
        {
        	fnfe.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getProperty(String key)
    {
        return applicationProps.getProperty(key);
    }	
}

