package com.immanuelnoel.testmongo.core.dataHandler;

import java.lang.reflect.Field;

import org.bson.types.ObjectId;

import com.immanuelnoel.testmongo.core.utils.PropertiesReader;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;


public class Data {

	private static Data instance = null;
	private DB db = null;
	
	int testPackageIndex;
	
	protected Data() {
		
		try{
			
			// Read config.properties
			PropertiesReader props = new PropertiesReader("src/config.properties");
			
			// Get DB credentials
			MongoClient mongoClient = new MongoClient(props.getProperty("DBHOST"), Integer.parseInt(props.getProperty("DBPORT")));
			db = mongoClient.getDB( "TestMongo" );
			
			// Get test package name
			String testPackageName = props.getProperty("TEST_PACKAGE");
			
			/* 
			 * Set Test Package Index. Pick the first package / class defined after the package specified in properties file
			 */
			String[] defaultTestPackages = testPackageName.split("\\.");
			testPackageIndex = defaultTestPackages.length;
			
		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
	}

	public static Data getInstance() {
		if(instance == null) {
			instance = new Data();
		}
		return instance;
	}
	
	// Get Data Count - Rewrite to fetch count from DB directly
	public int getDataCount(String testClassPackage) {
		
		String[] callStack = testClassPackage.split("\\.");
		
		String callingPage = "";
		for(int i = testPackageIndex; i < callStack.length - 1; i++){
			callingPage += "_" + callStack[i];
		}
		
		BasicDBList data = getDataforPage(callingPage);
		if(data != null){
			return data.size();
		} else {
			return 0;
		}
	}
	
	// Get Data
	public BasicDBList getData() {
		
		// Identify Caller
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String[] callStack = stackTraceElements[3].toString().split("\\.");
		
		String callingPage = "";
		for(int i = testPackageIndex; i < callStack.length - 3; i++){
			callingPage += "_" + callStack[i];
		}
		
		BasicDBList data = getDataforPage(callingPage);
		if(data != null){
			return data;
		} else {
			System.out.println("No data found for " + callingPage);
			return new BasicDBList();
		}
	}
	
	// Get Data Core Impl
	private BasicDBList getDataforPage(String callingPage){
		
		BasicDBList mongoData = new BasicDBList();
		
		// Create projection query
		DBCollection collection = db.getCollection("admin");
		
		// Get document for calling package
		BasicDBObject identifier = new BasicDBObject("appName", "moonDragon");
		DBCursor output = collection.find(identifier, new BasicDBObject(callingPage, 1));

		// Get data
		try {
			mongoData = (BasicDBList) output.toArray().get(0).get(callingPage);	
		} catch(IndexOutOfBoundsException e){
			System.out.println("Data not found for the Class " + callingPage+ ". Skipping execution");
		}
			
		return mongoData;
	}
	
	
	public void deleteAllData(){
		
		// Identify Caller
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String[] callStack = stackTraceElements[2].toString().split("\\.");
		
		String callingPage = "";
		for(int i = testPackageIndex; i < callStack.length - 3; i++){
			callingPage += "_" + callStack[i];
		}
		
		DBCollection collection = db.getCollection("admin");	
		
		// Delete Calling Document
		BasicDBObject identifier = new BasicDBObject("appName", "moonDragon");
		
		BasicDBObject deleteCommand = new BasicDBObject();
		deleteCommand.put( "$unset", new BasicDBObject(callingPage, 1));
		
		collection.update(identifier, deleteCommand);
	}
	
	// Persist data
	// Use NoSQL Manager for MongoDB to update / delete
	public Boolean inputData(Object item){
		
		Boolean returnVar = false;
		
		// Identify Caller
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String[] callStack = stackTraceElements[2].toString().split("\\.");
		
		String callingPage = "";
		for(int i = testPackageIndex; i < callStack.length - 3; i++){
			callingPage += "_" + callStack[i];
		}
		
		try {
			
			BasicDBObject dbItem = convertToBasicDBObject(item);
			
			DBCollection collection = db.getCollection("admin");	
			
			// Get document for calling package
			BasicDBObject identifier = new BasicDBObject("appName", "moonDragon");
			
			DBCursor output = collection.find(identifier);
			
			// If document not present
			if(!output.hasNext()) {
				
				// Create Document
				collection.insert(identifier);
			}
			
			// Add first item
			BasicDBObject updateCommand = new BasicDBObject();
			updateCommand.put( "$push", new BasicDBObject(callingPage, dbItem));
			
			WriteResult wrItem = collection.update(identifier, updateCommand);
			
			if(wrItem != null)
				returnVar = true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnVar;
	}
	
	private BasicDBObject convertToBasicDBObject(Object object){
		
		BasicDBObject result = new BasicDBObject();
		result.put("_id", new ObjectId());
		
		Field[] attributes =  object.getClass().getDeclaredFields();
		
		for (Field field : attributes) {
        	field.setAccessible(true);
        	try {
        		
        		Object subObject = field.get(object);
        		if(subObject instanceof Integer || subObject instanceof String || subObject instanceof Boolean){
        			result.put(field.getName(), subObject);
        		}
        		else {
        			result.put(field.getName(), convertToBasicDBObject(subObject));
        		}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
		return result;
	}
}
