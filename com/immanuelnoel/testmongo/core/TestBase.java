package com.immanuelnoel.testmongo.core;

import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestResult;

import com.immanuelnoel.testmongo.core.dataHandler.Data;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class TestBase implements IHookable{

	protected int invocationIndex = 0;
	protected BasicDBList DataList = new BasicDBList();
	
	@Override
	public void run(IHookCallBack arg0, ITestResult arg1) {
		this.invocationIndex = (int)arg1.getAttribute("currentInvocationCount");
		arg0.runTestMethod(arg1);
	}
	
	// Fetch data from DB for calling class
	protected void setup() {
		
		Data dataObj = Data.getInstance(); 
		DataList = dataObj.getData();
	}
	
	// Get instance of data, as per CurrentInvocationCount
	protected BasicDBObject getDataObject(){
		if(DataList.size() > 0) {
			return (BasicDBObject)DataList.get(invocationIndex);
		} else {
			System.out.println("No data found. Ensure super.setup() is called in the constructor");
			return new BasicDBObject();
		}
	}
	
	
}
