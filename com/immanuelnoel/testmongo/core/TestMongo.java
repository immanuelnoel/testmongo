package com.immanuelnoel.testmongo.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.TestNG;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.immanuelnoel.testmongo.core.listeners.InvocationListener;
import com.immanuelnoel.testmongo.core.listeners.Invocator;
import com.immanuelnoel.testmongo.core.utils.PropertiesReader;

public class TestMongo {

	public void runTestNGTest(Map<String,String> testngParams) {
		 
		PropertiesReader props = new PropertiesReader("src/config.properties");
		String testPackageName = props.getProperty("TEST_PACKAGE");
		String packagesToExecute = props.getProperty("PACKAGES_TO_EXECUTE");
		
		String[] defaultTestPackages = testPackageName.split("\\.");
		
		// Choose TestNG suiteName and testName based on TestNG test package name
		String suiteName = "";
		String testName = "";
		switch(defaultTestPackages.length) {
		
			case 0:
				suiteName = "TestMongo";
				testName = "TestMongo Tests";
				break;
				
			case 1:
				suiteName = "TestMongo";
				testName = (defaultTestPackages[0].compareTo("com") == 0 ? "TestMongo Tests" : defaultTestPackages[0]);
				break;
				
			case 2:
				suiteName = (defaultTestPackages[0].compareTo("com") == 0 ? "TestMongo" : defaultTestPackages[0]);
				testName = defaultTestPackages[1];
				break;
				
			default:
				suiteName = (defaultTestPackages[0].compareTo("com") == 0 ? defaultTestPackages[1] : defaultTestPackages[0]);
				testName = (defaultTestPackages[0].compareTo("com") == 0 ? defaultTestPackages[2] : defaultTestPackages[1]);
				break;
		}
		
		//Create an instance on TestNG
		 TestNG myTestNG = new TestNG();
		 
		//Create an instance of XML Suite and assign a name for it.
		 XmlSuite mySuite = new XmlSuite();
		 mySuite.setName(suiteName);
		 
		//Create an instance of XmlTest and assign a name for it.
		 XmlTest myTest = new XmlTest(mySuite);
		 myTest.setName(testName);
		 
		//Add any parameters that you want to set to the Test.
		 myTest.setParameters(testngParams);
		 
		// Pick the package housing testcases from properties file
		 
		 List<XmlPackage> myPackages = new ArrayList<XmlPackage> ();
		 myPackages.add(new XmlPackage(testPackageName + "." + packagesToExecute));
		 
		 //Assign that to the XmlTest Object created earlier.
		 myTest.setXmlPackages(myPackages);
		 
		//Create a list of XmlTests and add the Xmltest you created earlier to it.
		 List<XmlTest> myTests = new ArrayList<XmlTest>();
		 myTests.add(myTest);
		 
		//add the list of tests to your Suite.
		 mySuite.setTests(myTests);
		 
		//Add the suite to the list of suites.
		 List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
		 mySuites.add(mySuite);
		 
		//Set the list of Suites to the testNG object you created earlier.
		 myTestNG.setXmlSuites(mySuites);
		 
		 // Add listeners
		 myTestNG.setAnnotationTransformer(new Invocator());
		 myTestNG.addListener(new InvocationListener());
		 
		//invoke run() - this will run your class.
		 myTestNG.run();
		 
		}
		 
		public static void main(String args[]) {
		 
			TestMongo core = new TestMongo();
			 
			//This Map can hold your testng Parameters.
			Map<String,String> testngParams = new HashMap<String,String>();
			 
			core.runTestNGTest(testngParams);
		 
		}
}
